<?php

require 'DB.php';

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ReplacementDataSet;

class DBTest extends TestCase
{
    use TestCaseTrait;

    static private $pdo = null;
    private $conn = null;

    final public function getConnection()
    {
        $db = 'pdo_test';
        $user = 'admin';
        $pwd = 'password';

        if ($this->conn === null) {
            if (self::$pdo === null) {
                self::$pdo = new PDO('mysql:host=localhost;dbname=' . $db, $user, $pwd);
            }

            $this->conn = $this->createDefaultDBConnection(self::$pdo, $db);
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        return $this->createMySQLXMLDataSet(dirname(__FILE__) . '/_files/basic_table.xml');
    }

    public function testInsertRow()
    {
        $newUser = new DB();

        $newUser->insertRow("Emilia", "Klark", "mother@of.dragons");

        $expectedTable = $this->createMySQLXMLDataSet(dirname(__FILE__) . '/_files/add_row.xml');

        $expectedTableFixed = new ReplacementDataSet($expectedTable);
        $expectedTableFixed->addFullReplacement('##NOW##', date("Y-m-d H:i:s"));

        $db_dataset = $this->getConnection()->createDataSet(array('user'));

        $this->assertDataSetsEqual($expectedTableFixed, $db_dataset);

    }

    public function testUpdate()
    {
        $updUser = new DB();

        $updUser->where('email', 'noOscar@i.have');
        $updUser->update(['name' => 'Will', 'lastName' => 'Smith']);

        $expectedTable = $this->createMySQLXMLDataSet(dirname(__FILE__) . '/_files/update_row.xml');
        $db_dataset = $this->getConnection()->createDataSet(array('user'));

        $this->assertDataSetsEqual($expectedTable, $db_dataset);

    }

    public function testSearch()
    {
        $db = new DB();

        $db->search('name, email');
        $this->assertEquals('name, email', $db->selected);
        $this->assertEquals('SELECT name, email', $db->queryParams);
    }

    public function testWhere()
    {
        $db = new DB();

        $db->where('name', 'Leonardo');
        $this->assertEquals(array('name' => 'Leonardo'), $db->where);
    }

    public function testWhereDate()
    {
        $db = new DB();

        $db->whereDate('2020-01-19', '2020-01-30');
        $this->assertEquals(array('from' => '2020-01-19', 'to' => '2020-01-30'), $db->whereDate);
    }

    public function testOrWhereDate()
    {
        $db = new DB();

        $db->orWhereDate('2020-01-19', '2020-01-30');
        $this->assertEquals(array('from' => '2020-01-19', 'to' => '2020-01-30'), $db->whereDate);

        $db->orWhereDate('2020-02-12', '2020-02-29');
        $this->assertEquals(array('from' => '2020-02-12', 'to' => '2020-02-29'), $db->orWhereDate);
    }

    public function testOrWhere()
    {
        $db = new DB();

        $db->orWhere('lastName', 'Guy');
        $this->assertEquals(array('lastName' => 'Guy'), $db->where);

        $db->orWhere('lastName', 'DiCaprio');
        $this->assertEquals(array('lastName' => 'DiCaprio'), $db->or);


    }

    public function testAndWhere()
    {
        $db = new DB();

        $db->andWhere('lastName', 'Guy');
        $this->assertEquals(array('lastName' => 'Guy'), $db->where);

        $db->andWhere('lastName', 'DiCaprio');
        $this->assertEquals(array('lastName' => 'DiCaprio'), $db->and);
    }

    public function testLimit()
    {

        $db = new DB();

        $db->limit(5);
        $this->assertEquals(" LIMIT 5", $db->limit);

    }

    public function testOrderBy()
    {

        $db = new DB();

        $db->orderBy('email');
        $this->assertEquals("email", $db->orderBy);

    }

    public function testGet()
    {

        $db = new DB();

        $db->search('name, lastName, email')
            ->where('lastName', 'Smith')
            ->orWhere('lastName', 'Guy')
            ->andWhere('name', 'Will')
            ->limit(1, 2)
            ->orderBy('email')
            ->get();


        $queryTable = $this->getConnection()->createQueryTable(
            'user',
            "SELECT name, lastName, email FROM user WHERE lastName='Smith' OR lastName='Guy' AND name='Will' ORDER BY email LIMIT 1, 2"
        );

        $expectedTable = $this->createMySQLXMLDataSet(dirname(__FILE__) . '/_files/search_result.xml')
            ->getTable("user");

        $this->assertTablesEqual($expectedTable, $queryTable);

    }

    public function testIssetWhere()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetWhere());

        $db->where['name'] = 'Richard';
        $db->issetWhere();

        $this->assertEquals(" WHERE name=:name", $db->issetWhere());
    }

    public function testIssetWhereDate()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetWhereDate());

        $db->whereDate['from'] = '2019-12-31';
        $db->whereDate['to'] = '2020-01-20';
        $db->issetWhereDate();

        $this->assertEquals(" WHERE createdAt BETWEEN :from AND :to", $db->issetWhereDate());
    }

    public function testIssetOrWhereDate()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetOrWhereDate());

        $db->orWhereDate['from'] = '2019-12-31';
        $db->orWhereDate['to'] = '2020-01-20';
        $db->issetOrWhereDate();

        $this->assertEquals(" OR createdAt BETWEEN :orFrom AND :orTo", $db->issetOrWhereDate());
    }

    public function testIssetOr()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetOr());

        $db->or['name'] = 'Brad';
        $db->issetOr();

        $this->assertEquals(" OR name=:orname", $db->issetOr());
    }

    public function testIssetAnd()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetAnd());

        $db->and['name'] = 'Brad';
        $db->issetAnd();

        $this->assertEquals(" AND name=:andname", $db->issetAnd());
    }

    public function testIssetOrderBy()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetOrderBy());

        $db->orderBy = 'name';
        $db->issetOrderBy();

        $this->assertEquals(" ORDER BY name", $db->issetOrderBy());
    }

    public function testIssetLimit()
    {

        $db = new DB();

        $this->assertEquals("", $db->issetLimit());

        $db->limit = ' LIMIT 5,10';
        $db->issetLimit();

        $this->assertEquals(" LIMIT 5,10", $db->issetLimit());
    }

}