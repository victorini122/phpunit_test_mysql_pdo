<?php

class DB
{

    private $dsn = 'mysql:host=localhost;dbname=pdo_test';
    private $user = 'admin';
    private $pwd = 'password';
    private $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    public static $dbh = null;
    public static $sth = null;
    public $where;
    /**
     * @var string
     */
    public $queryParams;
    /**
     * @var string
     */
    public $selected;
    public $or;
    public $and;
    /**
     * @var string
     */
    public $limit;
    /**
     * @var string
     */
    public $orderBy;
    public $whereDate;
    /**
     * @var array
     */
    public $orWhereDate;
    public $params = array();

    function __construct()
    {

        self::$dbh = new PDO($this->dsn, $this->user, $this->pwd, $this->opt);

        return self::$dbh;

    }

    /**
     * Method allows to add new row to DB
     *
     * @param $name
     * @param $lastName
     * @param $email
     * @return bool
     */
    public function insertRow($name, $lastName, $email)
    {

        $createdAt = date("Y-m-d H:i:s");

        $allowed = array("name" => $name, "lastName" => $lastName, "email" => $email, "createdAt" => $createdAt);

        $query = "INSERT INTO user SET name=:name, lastName=:lastName, email=:email, createdAt=:createdAt";

        self::$sth = self::$dbh->prepare($query);

        return (self::$sth->execute((array)$allowed)) ? true : false;

    }

    /**
     *
     * Method save searched columns
     *
     * @param string $select
     * @return $this
     */
    public function search(string $select)
    {
        $this->selected = $select;
        $this->queryParams = "SELECT " . $select;

        return $this;
    }

    /**
     *
     * Method save data for 'WHERE' clause
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function where($key, $value)
    {
        if (!$this->whereDate) {
            $this->where = array($key => $value);
        }

        return $this;
    }

    /**
     *
     * Method save data for 'WHERE date BETWEEN ? AND ?' clause
     *
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function whereDate(string $from, string $to)
    {
        if (!$this->where) {
            $this->whereDate = array(
                'from' => $from,
                'to' => $to
            );
        }

        return $this;

    }

    /**
     *
     * Method save data for 'OR date BETWEEN ? AND ?' clause
     *
     * @param string $from
     * @param string $to
     * @return $this
     */
    public function orWhereDate(string $from, string $to)
    {
        $betweenDates = array(
            'from' => $from,
            'to' => $to
        );

        if ($this->whereDate) {
            $this->orWhereDate = $betweenDates;
        } else {
            $this->whereDate = $betweenDates;
        }


        return $this;
    }

    /**
     *
     * Method save data for 'OR ?=?' clause
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function orWhere($key, $value)
    {
        if ($this->where || $this->whereDate) {
            $this->or[$key] = $value;
        } else {
            $this->where = array($key => $value);
        }

        return $this;
    }

    /**
     *
     * Method save data for 'AND ?=?' clause
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function andWhere($key, $value)
    {
        if ($this->where || $this->whereDate) {
            $this->and[$key] = $value;
        } else {
            $this->where = array($key => $value);
        }

        return $this;
    }

    /**
     *
     * Method save data for 'LIMIT ?,?' clause
     *
     * @param $val1
     * @param null $val2
     * @return $this
     */
    public function limit($val1, $val2 = null)
    {

        $this->limit = " LIMIT " . $val1;
        $this->limit .= !is_null($val2) ? ", " . $val2 : "";

        return $this;

    }

    /**
     *
     * Method save data for 'ORDER BY ' clause
     *
     * @param string $orderBy
     * @return $this
     */
    public function orderBy(string $orderBy)
    {

        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     *
     * Method allows to update/change existing info in table
     *
     * @param array $updates
     * @return bool
     */
    public function update(array $updates)
    {

        $this->params = $updates;

        $setParams = implode(', ', array_map(
            function ($v, $k) {
                if (!empty($v))
                    return sprintf("%s=:%s", $k, $k);
            },
            $updates,
            array_keys($updates)
        ));

        $query = "UPDATE user SET " . $setParams;
        $query .= $this->issetWhere();
        $query .= $this->issetWhereDate();

        self::$sth = self::$dbh->prepare($query);

        return (self::$sth->execute($this->params)) ? true : false;

    }

    /**
     *
     * Method checks existence of $where;
     *
     * @return string
     */
    public function issetWhere()
    {

        $query = "";

        if ($this->where) {
            $query = " WHERE " . key($this->where) . "=:" . key($this->where);
            $this->params[key($this->where)] = $this->where[key($this->where)];
        }

        return $query;
    }

    /**
     *
     * Method checks existence of $whereDate;
     *
     * @return string
     */
    public function issetWhereDate()
    {
        $query = "";

        if ($this->whereDate) {
            $query .= " WHERE createdAt BETWEEN :from AND :to";
            $this->params['from'] = $this->whereDate['from'];
            $this->params['to'] = $this->whereDate['to'];
        }

        return $query;
    }

    /**
     *
     * Method checks existence of $orWhereDate;
     *
     * @return string
     */
    public function issetOrWhereDate()
    {

        $query = "";

        if ($this->orWhereDate) {
            $query .= " OR createdAt BETWEEN :orFrom AND :orTo";
            $this->params['orFrom'] = $this->orWhereDate['from'];
            $this->params['orTo'] = $this->orWhereDate['to'];
        }

        return $query;

    }

    /**
     *
     * Method checks existence of $or;
     *
     * @return string
     */
    public function issetOr()
    {
        $query = "";

        if ($this->or) {
            foreach ($this->or as $key => $value) {
                $query .= " OR " . $key . "=:or" . $key;
                $this->params['or' . $key] = $value;
            }
        }

        return $query;
    }

    /**
     *
     * Method checks existence of $and;
     *
     * @return string
     */
    public function issetAnd()
    {
        $query = "";

        if ($this->and) {
            foreach ($this->and as $key => $value) {
                $query .= " AND " . $key . "=:and" . $key;
                $this->params['and' . $key] = $value;
            }
        }

        return $query;
    }

    /**
     *
     * Method checks existence of $orderBy;
     *
     * @return string
     */
    public function issetOrderBy()
    {
        $query = "";

        if ($this->orderBy) {
            $query .= ' ORDER BY ' . $this->orderBy;
        }

        return $query;
    }

    /**
     *
     * Method checks existence of $limit;
     *
     * @return string
     */
    public function issetLimit()
    {

        $query = "";

        if ($this->limit) {
            $query .= $this->limit;
        }

        return $query;
    }

    /**
     *
     * Method allows to get searched info
     *
     * @return bool|PDOStatement|null
     */
    public function get()
    {

        $query = $this->queryParams . " FROM user";

        $query .= $this->issetWhere();
        $query .= $this->issetWhereDate();
        $query .= $this->issetOrWhereDate();
        $query .= $this->issetOr();
        $query .= $this->issetAnd();
        $query .= $this->issetOrderBy();
        $query .= $this->issetLimit();

        self::$sth = self::$dbh->prepare($query);
        self::$sth->execute($this->params);

        echo '<table cellspacing="0" border="1" cellpadding="10" ><tr>';

        $columns = explode(", ", $this->selected);

        foreach ($columns as $col) {
            echo '<td>';
            echo $col;
            echo '</td>';
        }

        echo '</tr>';
        foreach (self::$sth as $row) {
            echo '<tr>';
            foreach (array_values($row) as $val) {

                echo '<td>';
                echo $val;
                echo '</td>';

            }
            echo '</tr>';
        }
        echo '</table>';

        return self::$sth;
    }


}